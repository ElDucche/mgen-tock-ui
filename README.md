# Chatbot du Lab inno MGEN

## Pour l'installer :
`npm i mgen-tock-ui`
Cela installe la dépendance du projet mgen-tock-ui.

## Pour l'intégrer
Un seul composant TSX est utile dans ce projet, il s'agit du composant `<Chatbox />`.  
Il faut l'importer avec la ligne :  
`import {Chatbox} from 'mgen-tock-ui/src/Chatbox';`

Merci pour votre lecture.


