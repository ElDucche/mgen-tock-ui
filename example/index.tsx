import 'react-app-polyfill/ie11';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Chatbox } from '../src/Chatbox';

const App = () => {
  return (
    <div>
      <Chatbox />
    </div>
  );
};

ReactDOM.render(<App />, document.getElementById('root'));
