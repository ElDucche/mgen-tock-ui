export type UserMessage = {
    speaker: 'user';
    msg: string;
}

// Typer les réponse du bot :

export type Response = {
    responses : Message[];
}

export type Message = {
    text?: string;
    buttons?: Buttons;
    card?: Card;
    carousel?: Carousel;
    widget?: Widget;
    type: string;
    version: string;
}

interface PostbackButton {
    title: string;
    payload: string;
    type: "postback";
}

interface QuickReplyButton {
    title: string;
    payload: string;
    type: "quick_reply";
}

interface UrlButton {
    title: string;
    url: string;
    type: "web_url";
}

export type Button = PostbackButton | QuickReplyButton | UrlButton;

export type Buttons = Array<Button>;

export type Carousel = {
    cards : Card[];
}

export type Card = {
    title?: string;
    subtitle?: string;
    file: File;
    buttons: Buttons;
}

export type File = {
    url: string;
    name: string;
    type: string;
}

export type Widget = {
    data: Object;
    type: string;
}

