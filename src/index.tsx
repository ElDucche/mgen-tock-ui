import * as React from 'react'
import { Chatbox } from './Chatbox'

const Chatbot = () => {
    return (
        <Chatbox />
    )
}

export default Chatbot;