import React from "react";
import { useState } from "react";
import './styles.css';
import { BotResponse } from "./BotResponse";

export const Chatbox = () => {

    const processResponses = (responses: any) => {
      let processedResponse = {
        speaker: "bot"};
    
      responses.forEach((response:any) => {
        if (response.text) {
          //@ts-ignore
          processedResponse.text = response.text;
        } else if (response.card) {
          //@ts-ignore
          processedResponse.card = response.card;
        } else if (response.carousel && Array.isArray(response.carousel.cards)) {
          //@ts-ignore
          processedResponse.carousel = response.carousel;
        } else if (response.buttons) {
          //@ts-ignore
          processedResponse.buttons = response.buttons;
        }
      });
    
      return processedResponse;
    }
      // Gère l'ouverture/fermeture du Chat.
    const [isOpen, setIsOpen] = useState(false)
    // Permet à mes bulles d'avoir un unique key
    let chatCount = 0;
    // La c'est le tableau de Chat qui met à jour l'interface quand un message y est push.
    const [chatDialog, setChatDialog] = useState([]);
    // Mon Loader
    const [isLoading, setIsLoading] = useState(false);
    // Mega fonction 
    const discussing = async (event: any) => {
      event.preventDefault();
      const formData = new FormData(event.currentTarget);
      //@ts-ignore
      const entries = Object.fromEntries(formData.entries());
      const query = entries.query;
      const userDialog = {
        speaker: "user",
        msg: query
      }
      //@ts-ignore
     chatDialog.push(userDialog)
     setChatDialog([...chatDialog]);
     // Lancement de la requete.
     setIsLoading(true);
      const body = JSON.stringify({
          "query" : query,
          "userId" : "ID_0001"
      });
      const request = await fetch('https://bot.labinno-mtech.fr/io/app/labinno/web', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: body,
      }).then(res => res.json())
      const responsesData = request.responses;
      // Je dois modifier ça.
      const botDialog = processResponses(responsesData)
      //@ts-ignore
      chatDialog.push(botDialog)
      setIsLoading(false);
      setChatDialog([...chatDialog])
      //@ts-ignore
      document.getElementById('dial').value = "";
    };
  
    return (
      <div className={"container"}>
        <div className={"containerMain"}>
            {
                isOpen === true ? 
                <button onClick={() => setIsOpen(false)} className={"closeButton"}>
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" style={{width: 2+'rem' , height:2+'rem'}}>
                        <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
                :
  
                <button onClick={() => setIsOpen(true)} className={"openButton"} >
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" style={{width: 2+'rem' , height:2+'rem'}}>
                    <path strokeLinecap="round" strokeLinejoin="round" d="M20.25 8.511c.884.284 1.5 1.128 1.5 2.097v4.286c0 1.136-.847 2.1-1.98 2.193-.34.027-.68.052-1.02.072v3.091l-3-3c-1.354 0-2.694-.055-4.02-.163a2.115 2.115 0 01-.825-.242m9.345-8.334a2.126 2.126 0 00-.476-.095 48.64 48.64 0 00-8.048 0c-1.131.094-1.976 1.057-1.976 2.192v4.286c0 .837.46 1.58 1.155 1.951m9.345-8.334V6.637c0-1.621-1.152-3.026-2.76-3.235A48.455 48.455 0 0011.25 3c-2.115 0-4.198.137-6.24.402-1.608.209-2.76 1.614-2.76 3.235v6.226c0 1.621 1.152 3.026 2.76 3.235.577.075 1.157.14 1.74.194V21l4.155-4.155" />
                    </svg>
                </button>
            }
          
        </div>
            <div className={
              (isOpen === true) ? "chatbox" : ""
            } style={isOpen ? {} : {display: "none"}}>
                <div className={"chatboxTitle"}>
                    <h1>Mon Assistant MGEN*</h1>
                </div>
                  {/* Chatbox et affichage des messages. */}
                <div className={"chatboxDialog"} id='chat'>
                    {
                      // Je dois afficher le message du user instantannément.
                      //@ts-ignore
                      chatDialog.map((chat: any)=> {
                        if (chat.speaker === "user"){
                          return (
                            <div className={"chatbubbleUser"} key={chatCount++}>
                              <div className="chat-bubble">{chat.msg}</div>
                            </div>
                          )
                        }
                        else if (!isLoading && chat.speaker === "bot"){
                          return (
                            <BotResponse response={chat} key={chatCount++} />
                          )
                        }
                        else if (isLoading){
                          return (
                            <div className="chatbubbleBot">
                              ...
                            </div>
                          )
                        }  
                      })
                    }
                </div>

                {/* Input de ma Query */}
                <form className={"form"} onSubmit={discussing}>
                    <input id='dial' type="text" name='query' maxLength={250} className={"formInput"} placeholder="Écrivez ici..."/>
                    <button type='submit'>
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className={"sendButton"}>
                        <path strokeLinecap="round" strokeLinejoin="round" d="M6 12L3.269 3.126A59.768 59.768 0 0121.485 12 59.77 59.77 0 013.27 20.876L5.999 12zm0 0h7.5" />
                    </svg>
                    </button>
                </form>

            </div>
        </div>
    )
}
