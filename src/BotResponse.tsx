import React from "react";
import "./styles.css"

export const BotResponse = ({response} : {response: any}) => {
  
    return (
      <div className={"chatbubbleBot"}>
        {response.text && <div className='chat-bubble'>{response.text}</div>}
        {response.carousel && (
          <div className='chat-bubble'>
            {response.carousel.cards.map((card: any, index: number) => (
              <div key={index}>
                <h3>{card.title}</h3>
                <h4>{card.subTitle}</h4>
                {card.file?.url && (
                  <img src={card.file.url} alt={card.file.name} />
                )}
                {card.buttons.map((button:any) => (
                  <div className={"chatbubbleBotLink"}>
                    <a key={button.title} href={button.url} target={'_blank'} >
                      {button.title}
                    </a>
                  </div>
                ))}
              </div>
            ))}
          </div>
        )}
        {response.card && (
          <div className='chat-bubble'>
            <h3>{response.card.title}</h3>
            <h4>{response.card.subTitle}</h4>
            {response.card.file.url && (
              <img src={response.card.file.url} alt={response.card.file.name} />
            )}
            {response.card.buttons.map((button:any) => (
              <div className={"chatbubbleBotLink"}>
                <a key={button.title} href={button.url} target={'_blank'} >
                  {button.title}
                </a>
              </div>
            ))}
          </div>
        )}
        {response.buttons && (
          <div className='chat-bubble'>
            {response.buttons.map((button:any) => (
              <button key={button.title}>{button.title}</button>
            ))}
          </div>
        )}
      </div>
    );
  }